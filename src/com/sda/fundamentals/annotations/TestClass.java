package com.sda.fundamentals.annotations;

@ComponentInject(newClass = TestClass.class, name="testClass")
public class TestClass {
    private String testField;

    public TestClass(String testField) {
        this.testField = testField;
    }

    public String getTestField() {
        return testField;
    }

    @Deprecated
    void testMethod() {
        System.out.println("Deprecated Test Method");
    }

    @SuppressWarnings({"unchecked", "deprecation"}) // compilatorul presupune ca codul este corect(nu o sa rezulte in afisarea de warning uri, daca parametrii metodei nu sunt verificati)
    void printArray(String a, int ...args) {  // varargs
        for (int arg : args) {
            System.out.println(arg);
        }
    }

    @DangerousPieceOfCode(exceptionClass = RuntimeException.class)
    void methodThatThrowsAnException() {
        // a lot of lines of code
        throw new RuntimeException();
    }

    @Override // informeaza compilatorul ca metoda este delarata fie in clasa parinte, fie in interfata implementata;
    // ne ajuta sa evitam anumite greseli atunci cand suprascriem metodele
    public String toString() {
        return "TestClass{" +
                "testField='" + testField + '\'' +
                '}';
    }
}
