package com.sda.fundamentals.collections;

/**
 * Colectiile -> containere de date, folosite pentru:
 * ** stocare
 * ** manipulare
 * ** agregare
 * ** comunicare
 * Java ofera tipuri predefinite care reprezinta colectiile
 * Principalele tipuri sunt:
 * Collection -> The root interface in the collection hierarchy - reprezinta a colectie
 * List - extends Collection -> o lista ordonata de elemente, care poate contine elemente duplicate
 * Set - extends Collection -> un set de elemente, care NU contine duplicate, ordinea nu este importanta
 * Queue - extends Collection -> o coada, 2 tipuri de implementari: FIFO(ex. LinkedList), LIFO (ex. Stiva)
 * Map - reprezinta o structura de tipul keys - values; ex. dictionar de cuvinte
 *
 */
public class Main {
    public static void main(String[] args) {

    }
}
