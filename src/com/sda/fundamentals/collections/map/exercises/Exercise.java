package com.sda.fundamentals.collections.map.exercises;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Creati o mapa de persoane pentru fiecare caz de mai jos:
 * ** namesWithSurnames
 * ** namesWithAges
 * ** namesWithAListOfFriends
 * ** namesWithDetails - map of maps
 */
public class Exercise {
    public static void main(String[] args) {
        Map<String, String> namesWithSurnames = new HashMap<>();
        namesWithSurnames.put("Pop", "Ana-Maria");

        Map<String, Integer> namesWithAges = new HashMap<>();
        namesWithAges.put("Pop", 18);

        Map<String, List<String>> namesWithAListOfFriends = new HashMap<>();
        namesWithAListOfFriends.put("Anamaria Pop", List.of("Gabi", "Ale"));

        Map<String, Map<String, String>> namesWithDetails = new HashMap<>();
        namesWithDetails.put("Anamaria Pop", Map.of(
                "id", "123",
                "surnames", "Laura",
                "birthPlace", "Alexandria",
                "age", "18",
                "friendsNames", "Ion, Ana, Maria"
        ));

        Map<String, String> details = namesWithDetails.get("Anamaria Pop");
        details.get("id");
    }
}
