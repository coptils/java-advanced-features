package com.sda.fundamentals.collections.queue;

import java.util.LinkedList;

// FIFO (First In, First Out)
public class FifoQueueExample {
    public static void main(String[] args) {
        LinkedList<String> pokemons = new LinkedList<>();
        pokemons.offer("Pikachu");
        pokemons.offer("Charmander");
//        System.out.println(pokemons.peek());
        System.out.println(pokemons.poll());
        System.out.println(pokemons.poll());
    }
}
