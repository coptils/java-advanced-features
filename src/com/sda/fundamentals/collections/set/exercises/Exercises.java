package com.sda.fundamentals.collections.set.exercises;

import java.util.HashSet;
import java.util.Set;

/**
 * 1. Creeaza un set de culori; sterge o culoare din set; afiseaza setul inainte de stergerea culorii si dupa;
 */
public class Exercises {

    public static void main(String[] args) {
        Set<String> colors = new HashSet<>();
        colors.add("RED");
        colors.add("WHITE");
        colors.add("BLUE");

        System.out.println("Colors before removing element: ");
        for (String color : colors) {
            System.out.println(color);
        }

        colors.remove("RED");

        System.out.println("Colors after removing RED element: ");
        for (String color : colors) {
            System.out.println(color);
        }

    }

}
