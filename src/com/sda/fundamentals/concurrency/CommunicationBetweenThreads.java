package com.sda.fundamentals.concurrency;

public class CommunicationBetweenThreads {

    private boolean isDinnerReady;

    public synchronized void waitForDinner() throws InterruptedException {
        while (!isDinnerReady) {
            wait();  // thread ul asteapta pana o sa fie notificat
        }
        System.out.println("Enjoy Your meal!");
    }

    public synchronized void prepareDinner() {
        System.out.println("Dinner preparing...");
        this.setIsDinnerReady(true);
        notify(); // notify one single thread that is waiting for this resource
//      notifyAll(); // notify all
    }

    public void setIsDinnerReady(boolean isDinnerReady) {
        this.isDinnerReady = isDinnerReady;
    }
}
