package com.sda.fundamentals.concurrency.exercises.ex1;

public class CustomThread1 extends Thread {
    @Override
    public void run() {
        for (int i = 10; i < 30; i++) {
            if (i % 2 == 0) {
                System.out.println(Thread.currentThread().getName() + " " + i);
            }
        }
    }
}
