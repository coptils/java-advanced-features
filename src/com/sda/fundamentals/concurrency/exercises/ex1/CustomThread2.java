package com.sda.fundamentals.concurrency.exercises.ex1;

public class CustomThread2 extends Thread {
    @Override
    public void run() {
        for (int i = 50; i < 70; i++) {
            if (i % 2 == 0) {
                System.out.println(Thread.currentThread().getName() + " " + i);
            }
        }
    }
}
