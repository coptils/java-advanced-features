package com.sda.fundamentals.concurrency.exercises.ex4;

/**
 * Write a program which will synchronize access to a bank account. If any cyclical
 * Internet service wants to charge the account with a higher amount than currently
 * available, then the thread should be suspended. When additional money will be
 * transfered to the account, the thread should be raised.
 */
public class Account {
    private float sold;

    public Account(int sold) {
        this.sold = sold;
    }

    synchronized void transfer(float amount) {
        sold += amount;
        notify(); // comunica cu celelalte thread uri
        System.out.println(String.format("Transfer %f, sold: %f", amount, sold));
    }

    synchronized void pay(float amount) throws InterruptedException {
        while (amount > sold) {
            System.out.println("Not enough money! Waiting ... ");
            wait();
        }
        sold -= amount;
        System.out.println(String.format("Pay %f, sold: %f", amount, sold));
    }

}
