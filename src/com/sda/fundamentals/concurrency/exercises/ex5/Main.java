package com.sda.fundamentals.concurrency.exercises.ex5;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Scrie un program care sa execute doi algoritmi independenti de cautare a numarului 10 intr-un array vs o lista, pe doua thread uri separate.
 * Scopul principal al implementarii este sa returnati informatia despre algoritmul care a fost mai rapid.
 */
public class Main {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        Random random = new Random();
        int[] array = new int[10000];
        List<Integer> list = new ArrayList<>();

        for (int i = 0; i < 10000; i++) {
            array[i] = random.nextInt(20000);
            list.add(array[i]);
        }

        ExecutorService executorService = Executors.newFixedThreadPool(2);
        String result = executorService
                // executa task-urile date, returnand rezultatul celui care s-a completat primul cu succes
                .invokeAny(List.of(
                        new SearchInList(list),
                        new SearchInArray(array)
                ));

        System.out.println(result);
        executorService.shutdown();

    }
}
