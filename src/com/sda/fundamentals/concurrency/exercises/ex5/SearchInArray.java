package com.sda.fundamentals.concurrency.exercises.ex5;

import java.util.concurrent.Callable;

public class SearchInArray implements Callable<String> {

    private final int[] array;

    public SearchInArray(int[] array) {
        this.array = array;
    }

    public void search() {
        for (int number : array) {
            if (number == 10) {
                break;
            }
        }
    }

    @Override
    public String call() throws Exception {
        search();
        return "Number found in array";
    }
}
