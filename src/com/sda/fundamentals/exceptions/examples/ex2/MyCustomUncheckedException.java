package com.sda.fundamentals.exceptions.examples.ex2;

// custom unchecked exception
public class MyCustomUncheckedException extends RuntimeException {
    public MyCustomUncheckedException(String message) {
        super(message);
    }
}
