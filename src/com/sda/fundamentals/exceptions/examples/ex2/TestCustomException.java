package com.sda.fundamentals.exceptions.examples.ex2;

public class TestCustomException {
    public static void main(String[] args) {
        try {
            method1("       ");
        } catch (MyCustomCheckedException e) {
            System.out.println(e.getMessage());
        }

//        method2();
    }

    static String method1(String name) throws MyCustomCheckedException {
        if (name.isBlank()) {
            throw new MyCustomCheckedException("Custom checked exception: the name provided is not ok");
        } else {
            System.out.println("The name provided is ok");
            return name + "ok";
        }
    }

    static void method2() {
        throw new MyCustomUncheckedException("Unchecked exception custom");
    }
}
