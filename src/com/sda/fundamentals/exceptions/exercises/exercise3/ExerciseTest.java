package com.sda.fundamentals.exceptions.exercises.exercise3;

public class ExerciseTest {

    void testExerciseCustomException() throws CustomCheckedException {
        throw new CustomCheckedException("Throw a custom exception from test method");
    }

}
