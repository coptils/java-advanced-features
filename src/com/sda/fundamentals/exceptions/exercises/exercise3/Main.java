package com.sda.fundamentals.exceptions.exercises.exercise3;

/**
 * Create your own checked exception class using the extends keyword.
 * Write a constructor for this class that takes a String argument and stores it inside the object with a String reference.
 * Write a method that prints out the stored String. Create a try-catch clause to exercise your new exception.
 *
 * Write a test class with a method that throws an exception of the type created previously.
 * Try compiling it without an exception specification to see what the compiler says.
 * Add the appropriate exception specification. Try out your class and its exception inside a try-catch clause.
 */
public class Main {
    public static void main(String[] args) {
        try {
            throw new CustomCheckedException("Test my custom exception");
        } catch (CustomCheckedException e) {
            System.out.println(e.getMessage());
        }

        try {
            new ExerciseTest().testExerciseCustomException();
        } catch (CustomCheckedException ex) {
            System.out.println(ex);
        } finally {
            System.out.println("Finally block code");
        }
    }
}
