package com.sda.fundamentals.exceptions.exercises.exercise5;

/**
 * Define two custom exceptions: a runtime and a checked exception.
 * Create a class with two methods, f() and g(). In g(), throw an exception of a new type that you define - the runtime exception.
 * In f(), call g(), catch its exception and, in the catch clause, throw a different exception (of a second type that you define - checked exception).
 * Test your code in main( ).
 */
public class Main {
    public static void main(String[] args) throws CustomCheckedException {
        new Exercise().f();
    }
}
