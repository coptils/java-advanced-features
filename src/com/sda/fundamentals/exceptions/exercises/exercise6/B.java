package com.sda.fundamentals.exceptions.exercises.exercise6;

public class B extends A {
    @Override
    void method() throws CustomExceptionSubBase {
        throw new CustomExceptionSubBase("CustomExceptionSubBase");
    }
}
