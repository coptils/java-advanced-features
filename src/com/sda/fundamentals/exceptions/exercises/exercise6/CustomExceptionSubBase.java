package com.sda.fundamentals.exceptions.exercises.exercise6;

public class CustomExceptionSubBase extends CustomExceptionBase {
    public CustomExceptionSubBase(String message) {
        super(message);
    }
}
