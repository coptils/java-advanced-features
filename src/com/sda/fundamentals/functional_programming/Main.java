package com.sda.fundamentals.functional_programming;

public class Main {

    // OOP paradigm - clase si obiecte (abordare bottom-up => programul este impartit in obiecte in functie de problema)
    // Procedural paradigm - functii si proceduri (abordare top - down  => programul este impartita in bucatele micute pe baza functiilor)
    // Programarea proceduala -> functiile pot sa primeasca alte functii ca si input si sa returneze alte functii ca rezultat
    public static void main(String[] args) {

    }

}
