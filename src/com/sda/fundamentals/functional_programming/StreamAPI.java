package com.sda.fundamentals.functional_programming;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

// Stream -> pentru lucrul cu colectii, arrays, fisiere (mecanism de procesare)
// Stream = un set secvential de itemuri pe care putem aplica diverse operatii de manipulare a lor
// Stream -> permit paralelizarea usoara a operatiilor => procesarea mai rapida
public class StreamAPI {

    public static void main(String[] args) {
        // Streams - collect, findFirst, findAny

        List<String> names = Arrays.asList("Andrew", "Brandon", "Michael");
        Stream<String> namesStream = names.stream(); // convertim lista intr-un stream de date

        List<String> namesCopy = names.stream()
                .collect(Collectors.toList()); // operatie terminala

        Optional<String> firstNameOptional = names.stream()
                .findFirst(); // returneaza un Optional cu valoarea primului element, daca exista, altfel Optional.EMPTY
        firstNameOptional.ifPresentOrElse(
                System.out::println,
                () -> System.out.println("default")
                );

        Optional<String> firstNameOptional1 = names.stream()
                .findAny(); // returneaza prima valoare gasita sau empty Optional

        firstNameOptional1.ifPresentOrElse(
                System.out::println,
                () -> System.out.println("default")
        );

        // Streams - filter, map
        List<String> namesStartingWithA = names.stream()  // ["Andrew", "Brandon", "Michael"]
                .filter(name -> name.startsWith("A"))  // ["Andrew"]
                .collect(Collectors.toList());
        System.out.println(namesStartingWithA);

        List<Integer> namesLength = names.stream()
//                .map(name -> name.length())
                .map(String::length)
                .collect(Collectors.toList());
        System.out.println(namesLength);

        OptionalDouble averageNameLengthOptional = names.stream()   // ["Andrew", "Brandon", "Michael"]
                .mapToInt(String::length)  // [6, 7, 7]
                .average();  // operatie terminala la fel ca si collect
        averageNameLengthOptional.ifPresent(System.out::println);

        // Streams - allMatch, anyMatch
        boolean allNamesLengthIsGtThan3 = names.stream()
                .allMatch(name -> (name.length() > 3));  // opertie terminala
        System.out.println(allNamesLengthIsGtThan3);

        boolean thereIsANameWhichLengthIsGtThan3 = names.stream()
                .anyMatch(n -> n.length() > 3);
        System.out.println(thereIsANameWhichLengthIsGtThan3);

        // Streams - reduce method
        String nameConcatenation = names.stream()  // ["Andrew", "Brandon", "Michael"]   => "Andrew, Brandon, Michael"
                .reduce(
                        "",  // initial value
                        (result, element) -> (result.isBlank() ? "" : ", ") + element
                );
        System.out.println(nameConcatenation);

        // equivalent with the next code block

        String result = "";
        for(String element : names) {
//            if (result.isBlank()) {
//                result += "" + element;
//            } else {
//                result += ", " + element;
//            }
            result += (result.isBlank() ? "" : ", ") + element;
        }
        System.out.println(result);


        // suma elementelor dintr-un array de numere intregi
        int sumOfElements = Arrays.asList(1, 2, 3, 4, 5).stream()
                .reduce(
                        0,
                        (sum, element) -> sum + element
                );
        System.out.println("The sum of the array is: " + sumOfElements);


        // Streams - forEach, sorted
        names.stream()
                .forEach(System.out::println);   // the same as: names.forEach(System.out::println);

//        names.forEach(System.out::println);

        names.forEach(name -> System.out.print(name + ", "));

        System.out.println();
        List<Integer> sortedArrayOfNumbers = Arrays.asList(34, 21, 10, 50, 100, 3).stream()
//                .sorted((n1, n2) -> n1.compareTo(n2))
                .sorted(Comparator.naturalOrder()) // equivalent with the line from above
                .collect(Collectors.toList());
        System.out.println(sortedArrayOfNumbers);
    }

}
