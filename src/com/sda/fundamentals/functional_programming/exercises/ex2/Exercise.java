package com.sda.fundamentals.functional_programming.exercises.ex2;

import java.util.function.BiConsumer;

/**
 * Folosind expresiile lambda, realizati urmatoarele operatii:
 * addition, substraction, multiplication, division
 * Hint: use existing functional interfaces from Java 8 (Consumer)
 */
public class Exercise {
    public static void main(String[] args) {
        BiConsumer<Integer, Integer> addition = (op1, op2) -> System.out.printf("%d + %d = %d", op1, op2, op1 + op2);
        addition.accept(10, 20);

        BiConsumer<Integer, Integer> substraction = (op1, op2) -> System.out.printf("\n%d - %d = %d", op1, op2, op1 - op2);
        substraction.accept(20, 5);

        BiConsumer<Integer, Integer> multiplication = (op1, op2) -> System.out.printf("\n%d * %d = %d", op1, op2, op1 * op2);
        multiplication.accept(20, 5);

        BiConsumer<Integer, Integer> division = (op1, op2) -> System.out.printf("\n%d / %d = %d", op1, op2, op1 / op2);
        division.accept(15, 5);

    }
}
