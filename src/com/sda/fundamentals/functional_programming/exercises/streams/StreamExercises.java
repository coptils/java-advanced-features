package com.sda.fundamentals.functional_programming.exercises.streams;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Creati doua liste cu urmatoarele valori:
 * ["John", "Sarah", "Mark", "Tyla", "Ellisha", "Eamonn"]
 * [1, 4, 2346, 123, 76, 11, 0, 0, 62, 23, 50]
 * Realizati urmatoarele operatii:
 * a. sortati listele (alfabetic, crescator)
 * b. afisati doar acele nume care incep cu litera "E"
 * c. afisati valorile mai mari decat 30 si mai mici decat 200
 * d. afisati numele cu litera mare (uppercase)
 * e. stergeti prima si ultima litera, sortati si afisati numele
 */
public class StreamExercises {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("John", "Sarah", "Mark", "Tyla", "Ellisha", "Eamonn");
        List<Integer> numbers = List.of(1, 4, 2346, 123, 76, 11, 0, 0, 62, 23, 50);

        // a. sortati listele (alfabetic, crescator)
        System.out.println(
                names.stream()
                        .sorted(Comparator.naturalOrder())
                        .collect(Collectors.toList())
        );

        System.out.println(
                numbers.stream()
                        .sorted(Comparator.naturalOrder())
                        .collect(Collectors.toList())
        );

        // b. afisati doar acele nume care incep cu litera "E"
        System.out.println(
                names.stream()
                        .filter(n -> n.startsWith("E"))
                        .collect(Collectors.toList())
        );

        // c. afisati valorile mai mari decat 30 si mai mici decat 200
        System.out.println(
                numbers.stream()
                        .filter(number -> number > 30 && number < 200)
                        .collect(Collectors.toList())
        );


        // d. afisati numele cu litera mare (uppercase)
        System.out.println(
                names.stream()
                        .map(String::toUpperCase)
                        .collect(Collectors.toList())
        );


        // e. stergeti prima si ultima litera, sortati si afisati numele
        System.out.println(
                names.stream()
                        .map(n -> n.substring(1, n.length() - 1))
                        .sorted(Comparator.naturalOrder())
                        .collect(Collectors.toList())
        );

    }
}
