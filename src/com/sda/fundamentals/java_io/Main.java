package com.sda.fundamentals.java_io;

import java.io.*;
import java.nio.CharBuffer;
import java.nio.IntBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        File absoluteFile = new File("/home/coptils/Desktop/sda_courses/coding/java-advanced-features/absoluteFile.txt");
        File relativeFile = new File("relativeFile.txt");
        // vs
        Path absPath = Paths.get("/home/coptils/Desktop/sda_courses/coding/java-advanced-features/absoluteFile.txt");
        Path relPath = Paths.get("relativeFile.txt");

        readWriteByteOfData();

        readWriteCharByChar();

        readWriteLines();

        writeReadNioBuffer();

        createNioFileAtPath();

        serializationOfBookIntoFile(); // conversia unui obiect intr-un sir de biti

        deserializationOfBookFromFile(); // conversia din sir de biti intr-un obiect
    }

    private static void readWriteByteOfData() throws IOException {
        FileInputStream in = new FileInputStream("resources/user.txt");
        FileOutputStream out = new FileOutputStream("resources/user_output1.txt");
        int c;
        while ((c = in.read()) != -1) {
            out.write(c);
        }

        in.close();
        out.close();
    }

    private static void readWriteCharByChar() throws IOException {
        FileReader in = new FileReader("resources/user.txt");
        FileWriter out = new FileWriter("resources/user_output2.txt");

        int nextChar;
        while ((nextChar = in.read()) != -1) {
            out.append((char) nextChar);
        }

        in.close();
        out.close();
    }

    private static void readWriteLines() throws IOException {
        BufferedReader in = new BufferedReader(new FileReader("resources/user.txt"));
        BufferedWriter out = new BufferedWriter(new FileWriter("resources/user_output3.txt"));

        String line;
        while ((line = in.readLine()) != null) { // citirea linie cu linie pana la sfarsitul fisierului
            out.write(line);
            out.newLine(); // pentru ca fiecare linie sa se scrie pe un nou rand
        }

        // inchiderea conexiunilor
        in.close();
        out.close();

        // vs

        // try with resources, inchide automat conexiunile dupa iesirea din try
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("resources/user.txt"))) { // citirea din fisier
            String fileLine;
            while ((fileLine = bufferedReader.readLine()) != null) {
                System.out.println(fileLine);
            }
        }
        // nu mai trebuie sa inchidem conexiunile, au fost inchise automat de catre try

        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("resources/user_output4.txt"))) { // scrierea in fisier care nu exista - o sa se creeze unul nou
            String fileLine = "file line";
            bufferedWriter.write(fileLine);
        }

        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("resources/user_output4.txt", true))) { // scrierea intr-un fisier existent, se deschide fisierul existent si apoi se adauga in el
            String fileLine = "\nappended file line";
            bufferedWriter.write(fileLine);
        }

    }

    private static void writeReadNioBuffer() {
        ///////////////////////////////////////////
        CharBuffer buffer = CharBuffer.allocate(8); // alocam memorie pentru 8 caractere
        String text = "sda";

        for (int i = 0; i < text.length(); i++) {
            char ch = text.charAt(i); // returneaza caracterul aflat la indexul i, indecsii pornesc de la 0
            buffer.put(ch);
        }

        System.out.println("Position after data is written into buffer: " + Arrays.toString(buffer.array()));  // '\u0000' ascii code for null character


        ////////////////////////////////////////////
        // StringBuffer este ca un String, doar ca este mutable -> el poate fi modificat, este thread safe
        StringBuffer stringBuffer = new StringBuffer("Testing string buffer - sda courses");
        stringBuffer.append("\n");
        stringBuffer.append("another string on a new line");
        System.out.println(stringBuffer);

        //vs

        // String is immutable -> odata creat un string, el nu mai poate fi modificat
        String string = "ana are mere";
        System.out.println(string);
        string = "si pere";
        System.out.println(string);

        /////////////////////////////////////////////
        IntBuffer intBuffer = IntBuffer.allocate(10);
        intBuffer.put(1);
        intBuffer.put(2);
        intBuffer.put(3);
        System.out.println(Arrays.toString(intBuffer.array()));

    }

    private static void createNioFileAtPath() throws IOException {
        Path path = Paths.get("resources/data.txt");
        Files.createFile(path); // creeaza un fisier la path ul dat la linia 71 daca nu exista

        Path pathDir = Paths.get("resources/testDir");
        Files.createDirectory(pathDir);
        System.out.println("Is directory the file checked ? " + Files.isDirectory(pathDir));

        Files.write(path, "A long time ago in a galaxy far, far away....".getBytes(), StandardOpenOption.WRITE);
        Files.write(path, "in a galaxy far, ".getBytes(), StandardOpenOption.APPEND);
        Files.write(path, "far away....\n".getBytes(), StandardOpenOption.APPEND);

        List<String> fileLines = Arrays.asList("new line", "another new line", "another");
        Files.write(path, fileLines, StandardOpenOption.APPEND);

        List<String> lines = Files.readAllLines(path); // toate liniile din fisier
        for (String l : lines) {
            System.out.println(l);
        }
        Files.deleteIfExists(path); // sterge fisierul creat anterior
        Files.deleteIfExists(pathDir);
    }

    private static void serializationOfBookIntoFile() throws IOException {
        Book book = new Book(1, "Dracula", "V.G");

        // try from Exception Handling
//        try {
//            // do something
//        } catch(Exception exception1) {
//            // do another thing
//        }

        // try with resources
        try (FileOutputStream fileOutputStream = new FileOutputStream("resources/book_serialized.txt");
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)) {
            objectOutputStream.writeObject(book);
        }
        // try with resources - o sa inchida automat conexiunile pentru noi

    }

    private static void deserializationOfBookFromFile() throws IOException, ClassNotFoundException {
        Book book;
        // try with resources
        try (FileInputStream fileInputStream = new FileInputStream("resources/book_serialized.txt");
             ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)) {
            book = (Book) objectInputStream.readObject();
        }
        System.out.println("Book id: " + book.getId() + "; title: " + book.getTitle() + "; author: " + book.getAuthor());
    }

}
