package com.sda.fundamentals.java_io.exercises.ex3;

import java.io.File;

/**
 * Afiseaza toate fisierele/directoarele de la o anumita cale.
 */
public class Main {

    public static void main(String[] args) {
        File file = new File("/home/coptils/Desktop/sda_courses/coding/java-advanced-features");
        String[] fileList = file.list();

        for (String name : fileList) {
            System.out.println(name);
        }
    }

}
