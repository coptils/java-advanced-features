package com.sda.fundamentals.oop.composition;

public class Dog {
    private String name;
    private String color;
    private Muzzle muzzle;

    public Dog(String name, String color, Muzzle muzzle) {
        this.name = name;
        this.color = color;
        this.muzzle = muzzle;
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }

    public Muzzle getMuzzle() {
        return muzzle;
    }

}
