package com.sda.fundamentals.oop.encapsulation;

public class BankAccount {
    private double sum;
    private String iban;
    private String accountPassword;

    public BankAccount(String iban) {
        this.iban = iban;
        sum = 0;
    }

    public double getSum() {
        return sum;
    }

    public String getIban() {
        return iban;
    }

    public void setSum(double sum) {
        if (sum > 0) {
            this.sum = sum;
        }
    }

    public void setAccountPassword(String accountPassword) {
        this.accountPassword = accountPassword;
    }

}
