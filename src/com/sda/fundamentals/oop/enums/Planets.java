package com.sda.fundamentals.oop.enums;

public enum Planets {
    PLUTO(100, "Small Pluto"),
    MARTE(200, "Huge Marte"),
    JUPITER(300, "Jupiter");

    private String name;
    private int size;

    Planets(int size, String name) {
        this.size = size;
        this.name = name;
    }

    public void distanceFromEarth() {
        System.out.println("the distance is ...");
    }

    @Override
    public String toString() {
        return "Planet{" +
                "name=" + name +
                ";size=" + size +
                '}';
    }
}
