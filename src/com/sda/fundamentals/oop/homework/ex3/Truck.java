package com.sda.fundamentals.oop.homework.ex3;

public class Truck extends Car {

    private int weight;

    public Truck(int speed, double regularPrice, String color) {
        super(speed, regularPrice, color);
    }

    @Override
    public double getSalePrice() {
        if (weight > 2000) {
            return super.getSalePrice() - 0.1 * super.getSalePrice();
        }
        return super.getSalePrice() - 0.2 * super.getSalePrice();
    }
}
