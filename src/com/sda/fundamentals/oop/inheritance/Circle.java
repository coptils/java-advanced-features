package com.sda.fundamentals.oop.inheritance;

// child class, subclass, derived class
public class Circle extends Shape {
    private double radius;

    public Circle(double radius) {
        super();
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public void draw() {
        System.out.println("Drawing a circle");
    }

    public Double area() {
        return 2*Math.PI*radius;
    }

    public Double circumference() {
        return 2*radius;
    }

    @Override
    public String toString() {
        return super.toString() +
                "Circle{" +
                "radius=" + radius +
                '}';
    }
}
