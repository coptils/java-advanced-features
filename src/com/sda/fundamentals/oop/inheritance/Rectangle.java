package com.sda.fundamentals.oop.inheritance;

// child class, subclass, derived class
public class Rectangle extends  Shape {
    private double width;
    private double height;

    public Rectangle(double width, double height) {
        super();
        this.width = width;
        this.height = height;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    // this could me made abstract and moved in the parent class
    public void draw() {
        System.out.println("Drawing a rectangle");
    }

    public Double area() {
        return width * height;
    }

    public Double circumference() {
        return 2*width + 2*height;
    }

    @Override
    public String toString() {
        return super.toString() +
                "Rectangle{" +
                "width=" + width +
                ", height=" + height +
                '}';
    }
}
