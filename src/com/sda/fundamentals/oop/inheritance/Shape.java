package com.sda.fundamentals.oop.inheritance;

// parent class, superclass, base class
public class Shape {
    private double x;
    private double y;
    private String color;

    public Shape() {
        this.color = "red";
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void move(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Shape{" +
                "x=" + x +
                ", y=" + y +
                ", color='" + color + '\'' +
                '}';
    }
}
