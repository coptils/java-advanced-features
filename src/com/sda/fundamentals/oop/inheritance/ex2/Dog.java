package com.sda.fundamentals.oop.inheritance.ex2;

public class Dog extends Animal {

    private String color;
    private String name;

    public Dog(boolean veg, String food, int legs) {
        super(veg, food, legs);
        this.color = "red";
        this.name = "dog";
    }

    @Override
    public void yieldVoice() {
        System.out.println("Ham ham");
    }

    public Dog(boolean veg, String food, int legs, String color, String name) {
        super(veg, food, legs);
        this.color = color;
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
