package com.sda.fundamentals.oop.interfaces;

public class Cat implements AnimalInterface, AnimalGenericInterface {

    @Override
    public void food() {

    }

    @Override
    public void legs() {

    }
}
