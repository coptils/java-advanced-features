package com.sda.fundamentals.oop.interfaces;

public abstract class GenericAnimal {
    public abstract void food();
}
