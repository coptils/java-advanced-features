package com.sda.fundamentals.oop.interfaces;

// interface -> cannot be instantiated
// no put sa aibe field-uri
// metodele sunt public si abstract by default
// poate sa includa doar field uri si metode statice
// o clasa poate sa implementeze mai multe interfete
// poate sa include metode default - au body (version > Java 8)
// pot fi extinse folosind cuvantul "implements"
public interface InterfaceExample {

//    public String test; // won't compile
    public static final String TEST_CONSTANT = "test constant value"; // constanta

    String doSomething();
    void executeSomeCode();

//    void nonStaticMethodWithBody() {}   // incorrect declaration of method for interfaces

    static void methodWithBody() {
        System.out.println("Run method with body");
    }

    default void defaultMethodWithBody() {
        System.out.println("Default method that can have body");
    }

}
