package com.sda.fundamentals.oop.overloadingVsOverriding;

import java.util.Objects;

public class Car extends OverridingVsOverloading{
    private int maxSpeed;
    private String name;

    public Car(int maxSpeed, String name) {
        this.maxSpeed = maxSpeed;
        this.name = name;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (!(o instanceof Car)) return false;
        Car car = (Car) o;
        return
//                getMaxSpeed() == car.getMaxSpeed()
//                &&
                getName().equals(car.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMaxSpeed(), getName());
    }

    @Override
    public String toString() {
        return "Car{" +
                "maxSpeed=" + maxSpeed +
                ", name='" + name + '\'' +
                '}';
    }
}
