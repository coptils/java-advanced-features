package com.sda.fundamentals.oop.overloadingVsOverriding;

public class Test {
    public static void main(String[] args) {
        Car car1 = new Car(180, "Volvo");
        Car car2 = new Car(260, "Mercedes");
        Car car3 = new Car(280, "Volvo");

//        System.out.println("Car1 and Car2 are equals? " + car1.equals(car2));
        System.out.println("Car1 and Car2 are equals? " + car1.equals(car3));
    }
}
