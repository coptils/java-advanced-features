package com.sda.fundamentals.oop.packagesExample.example;

public class Test {

    public void runTest() {
        System.out.println("Test from example package");

//        Main.print();
    }

    void runTestDefaultAccess() {
        System.out.println("Method with default aaccess modifier");
    }

    protected void runTestProtectedAccess() {
        System.out.println("Method with default protected modifier");
    }

    private void privateMethod() {

    }

}
