package com.sda.fundamentals.oop.recap.ex1;

//public final class Point {   // final class cannot be inherited ? -> nu poate avea subclase
// why are they good?
// security -> unele metode pot astepta ca parametru un obiect al unei anumite clase si nu al unei subclase (cum se poate realiza in cazul polimorfismului),
// dar tipul exact al unui obiect nu poate fi aflat cu exactitate decat in momentul executiei;
public class Point {
    private int x; // x-coordinate
    private int y; // y-coordinate

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "Point: (" + x + "," + y + ")";
    }
}
