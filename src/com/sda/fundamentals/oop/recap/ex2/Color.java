package com.sda.fundamentals.oop.recap.ex2;

public enum Color {
    RED,
    DARK_RED,
    BLUE,
    DARK_BLUE,
    YELLOW,
    WHITE,
    BLACK,
    PINK,
    PURPLE
}
