package com.sda.fundamentals.oop.recap.ex3;

public interface Resizable {

    double resize(int percent);

}
