package com.sda.fundamentals.overall_exercise;

import java.util.List;

public class Budget<E> {
    private E value;

    public Budget(E value) {
        this.value = value;
    }

    public E getValue() {
        return value;
    }

    public void displayTotalBudget(List<Employee> employees) {
        Thread thread = new Thread(() -> {
            while(true) {
                double totalValue = employees.stream()
                        .map(Employee::getEarnings)
                        .reduce(0.0, Double::sum);
                System.out.println("The total budget has value: " + totalValue + " " + this.value);
                try {
                    Thread.sleep(50_000);
                } catch (InterruptedException e) {
                    System.out.println(e.getMessage());
                }
            }
        });
        thread.start();
    }

}
