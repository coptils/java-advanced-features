package com.sda.fundamentals.reflection;

import com.sda.fundamentals.reflection.car.Car;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/**
 * Reflection -> un mecanism puternic pus la dispozitie de pachetul java.lang.reflect (Reflection API)
 * Este mecanismul prin care o clasa/interfata sau obiect "reflecta" la momentul executiei structura lor interna
 * Reflection ne permite sa manipulam aproape orice element al unei clase;
 *
 * Pune la dispozitie:
 * * determinarea clasei unui obiect
 * * aflarea unor informatii despre o clasa (modificatori, superclasa, constructori, metode)
 * * instantierea unor clase a caror nume este cunoscut abia la executie
 * * invocarea metodelor unui obiect a carui nume este stiut abia la executie
 * * setarea sau aflarea atributelor unui obiect, chiar daca numele lor este stiut abia la executie
 *
 *  Utilizare in practica:
 * * implementarea in tools-uri de analiza a codului
 * * fromework -uri pentru implementarea dependency injection-ului (mai multe detalii la design patterns)
 * * implementarea mecanismului de serializare/deserializare al datelor (data viitoare :) )
 */
public class Reflection {
    public static void main(String[] args) throws Exception {
        Class<?> clazz = Class.forName("com.sda.fundamentals.reflection.car.Car");

        // create an instance from the Car class
        // Car car = new Car(); car.getType();   // by constructor
        Car myCarInstance = (Car) clazz.getDeclaredConstructor().newInstance();  // by reflection
        System.out.println("Type of the car is: " + myCarInstance.getType());  // calling a method on the instance created

        // make a private method accessible and call it
        Method privateMethod = clazz.getDeclaredMethod("getPrivateDetails");
        privateMethod.setAccessible(true);
        System.out.println(privateMethod.invoke(myCarInstance));


//        new Car().getPrivateDetails();  // not posible due to private access modifier
        // calling a method by using reflection
        Method getTypeMethod = clazz.getDeclaredMethod("getType");
        System.out.println("Car type is: " + getTypeMethod.invoke(clazz.getDeclaredConstructor().newInstance()));

        // get details about the class by using reflection
        Car car = new Car();
        car.getClass(); // aflarea instantei Class
        System.out.println("car instance is of type:" + car.getClass().getSimpleName());
        System.out.println("the superclass is:" + car.getClass().getSuperclass().getSimpleName());
        System.out.println("the package of the Capriciosa class is:" + car.getClass().getPackageName());

        int modifiers = clazz.getModifiers();

        System.out.println("the modifier acces of car class is public? " + Modifier.isPublic(modifiers));
    }
}
